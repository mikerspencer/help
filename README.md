# Help repository

This repo contains a [wiki](https://bitbucket.org/sruclees/help/wiki/Home), with tips and tricks for working with data and code.

### Contact

If you would like specific help, or think a topic should be added here, please [create an issue](https://bitbucket.org/sruclees/help/issues?status=new&status=open) or contact Mike Spencer.